/*!
 * Simple pagination of tables
 * Version: 1.0.0
 * Author: Sam Coult <sam@coult.com>
 * License: GNU GENERAL PUBLIC LICENSE Version 3
 */
(function ($) {

    $.fn.paginatable = function (options) {

        // Set some defaults
        let defaults = {
            rowsPerPage: 10,
            navID: "nav",
            navClass: "tablePagNav pagination pagination-sm pull-right",
            navLinkClass: "pagNavLink"
        };

        //Merge the supplied options with the defaults
        let settings = $.extend(defaults, options);

        return this.each(function(idx) {

            let theTable = $(this);
            let theRows = theTable.find("tbody tr");
            let rowsShown = settings.rowsPerPage;
            let rowsTotal = theRows.length;
            let numPages = rowsTotal / rowsShown;

            if (numPages < 1) {
                return this;
            }

            let thisNavID = settings.navID + '-' + idx;

            //Lets set up the pagination links
            theTable.after('<ul id="' + thisNavID + '" class="' + settings.navClass + '" ></ul>');

            for (i = 0; i < numPages; i++) {
                let pageNum = i + 1;
                $('#' + thisNavID).append('<li><a href="#" class="' + settings.navLinkClass + '" rel="' + i + '">' + pageNum + '</a></li>');
            }
            theRows.hide();
            theRows.slice(0, rowsShown).show();
            $('#' + thisNavID + ' a:first').addClass('active');

            //Set up the handler for the links
            $('#' + thisNavID + ' a').bind('click', function (e) {
                e.preventDefault();
                $('#' + thisNavID + ' a.' + settings.navLinkClass).removeClass('active');
                $(this).addClass('active');
                let currPage = $(this).attr('rel');
                let startItem = currPage * rowsShown;
                let endItem = startItem + rowsShown;
                theRows.css('opacity', '0.0').hide().slice(startItem, endItem).css('display', 'table-row').animate({opacity: 1}, 300);
            });
        });
    };
}(jQuery));